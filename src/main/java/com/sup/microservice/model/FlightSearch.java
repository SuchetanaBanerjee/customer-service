package com.sup.microservice.model;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;


public class FlightSearch {

    private Long id;
    private Long flightId;
    private String flightName;
    private int availability;
    private Date start_date;

    public FlightSearch() {

    }
    public FlightSearch(Long flightId,String flightName, int availability, Date start_date) {
        this.flightId = flightId;
        this.flightName = flightName;
        this.availability = availability;
        this.start_date = start_date;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Long getFlightId() {	return flightId;	}

    public void setFlightId(Long flightId) {this.flightId = flightId;}


    public String getFlightName() {
        return flightName;
    }
    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }


    public int getAvailability() {	return availability; }
    public void setAvailability(int availability) {
        this.availability = availability;
    }


    public Date getStart_date() {
        return start_date;
    }
    public void setStart_date(Date start_date) {
        this.start_date = start_date;
    }

    @Override
    public String toString() {
        return "FlightSearch{" +
                "id=" + id +
                ", flightId=" + flightId +
                ", flightName='" + flightName + '\'' +
                ", availability=" + availability +
                ", start_date=" + start_date +
                '}';
    }
}

