package com.sup.microservice.feign.fallback;


import com.sup.microservice.feign.client.FlightSearchClient;
import com.sup.microservice.model.FlightSearch;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

@Component
public class FlightSearchFallback implements FlightSearchClient {

    @Override
    public ResponseEntity<FlightSearch> getFlightByFlightId(Long id) {
        //return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        return new ResponseEntity("Server is not UP !!",HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
