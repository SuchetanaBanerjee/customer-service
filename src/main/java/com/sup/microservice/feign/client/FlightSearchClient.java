package com.sup.microservice.feign.client;


import com.sup.microservice.feign.config.CustomFeignConfig;
import com.sup.microservice.feign.fallback.FlightSearchFallback;
import com.sup.microservice.model.FlightSearch;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name="flight-search", configuration = CustomFeignConfig.class,url="http://localhost:8081/flight-search",fallback = FlightSearchFallback.class)
public interface FlightSearchClient {

   /* @RequestMapping(value="/getAvailabilityById/{id}", method = RequestMethod.GET)
    public int getAvailabilityById(@PathVariable long id) ;*/

    @GetMapping( value="/flightSearch/{id}")
    public ResponseEntity<FlightSearch> getFlightByFlightId(@PathVariable Long id);

    }


